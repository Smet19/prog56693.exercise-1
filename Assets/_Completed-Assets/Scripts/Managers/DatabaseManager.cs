using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

public static class DatabaseManager
{
    // Enum-like functionality
    public class TableName
    {
        private TableName(string _value) { Value = _value; IsRound = false; }
        private TableName(string _value, bool _isRound) { Value = _value; IsRound = _isRound; }
        public string Value { get; private set; }
        public bool IsRound { get; private set; }

        public static TableName Round1 { get { return new TableName("Round1", true); } }
        public static TableName Round2 { get { return new TableName("Round2", true); } }
        public static TableName Round3 { get { return new TableName("Round3", true); } }
        public static TableName TotalShots { get { return new TableName("TotalShots"); } }
        public static TableName DeathCoord { get { return new TableName("DeathCoord"); } }

    }

    public class Highscore
    {
        [BsonId]
        public int id { get; set; }
        public string Username { get; set; }
        public float Accuracy { get; set; }
        public int TotalRoundsWon { get; set; }
    }

    public static class MongoDB
    {
        private static int myId = 991647578;
        private static string myUsername = "Mikhail Smetankin";
        private static string connectionString = "mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/" + 
                                                "GDAP_Exercise?retryWrites=true&w=majority";
        public static Highscore GetHighScore()
        {
            MongoClient client = new MongoClient(connectionString);
            var db = client.GetDatabase("GDAP_Exercise");

            var coll = db.GetCollection<Highscore>("Highscores").Find(u => u.id == myId);
            if(coll.CountDocuments() > 0)
            {
                Highscore highscore = coll.Single();
                return highscore;
            }
            return null;
        }

        public static void Update(float _accuracy, int _TotalRoundsWon)
        {
            MongoClient client = new MongoClient(connectionString);
            var db = client.GetDatabase("GDAP_Exercise");

            var highScore = db.GetCollection<Highscore>("Highscores").Find(u => u.Username == myUsername).Limit(1).Single();

            highScore.Accuracy = (highScore.Accuracy + _accuracy * 100) / 2;
            highScore.TotalRoundsWon += _TotalRoundsWon;

            var update = Builders<Highscore>.Update.Set("Accuracy", highScore.Accuracy);

            var coll = db.GetCollection<Highscore>("Highscores");
            coll.UpdateOne(i => i.Username == highScore.Username, update);

            update = Builders<Highscore>.Update.Set("TotalRoundsWon", highScore.TotalRoundsWon);
            coll.UpdateOne(i => i.Username == highScore.Username, update);
        }
    }

    public static class SQLite
    {
        private static string connectionString = "URI=file:" + Application.dataPath + "/Database/TankStats.db";

        // Reads largest MatchID from Round1 table, returns incremented value
        public static int ReadMatchID()
        {
            int MatchId = 0;
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = "SELECT MatchID FROM Round1 ORDER BY MatchID DESC LIMIT 1";

                IDataReader reader = dbcmd.ExecuteReader();
                while (reader.Read())
                {
                    MatchId = reader.GetInt32(0);
                }

                reader.Close();
                dbcmd.Dispose();
                connection.Close();

            }
            return ++MatchId;
        }

        // Insert information about current round for each tank
        public static void InsertRound(int _plShots, int _aiShots, float _time, float _plDist, float _aiDist, TableName _tableName)
        {
            if (!_tableName.IsRound)
            {
                throw new System.ArgumentException($"{_tableName.Value} is not a Round table");
            }

            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                IDbCommand insCMD = connection.CreateCommand();
                insCMD.CommandText = $"INSERT INTO {_tableName.Value}(MatchID, PLShots, AIShots, Time, PLDist, AIDist)" +
                                    $"VALUES (@MatchID, @PLShots, @AIShots, @Time, @PLDist, @AIDist);";

                IDbDataParameter MatchID = insCMD.CreateParameter();
                IDbDataParameter PLShots = insCMD.CreateParameter();
                IDbDataParameter AIShots = insCMD.CreateParameter();
                IDbDataParameter Time = insCMD.CreateParameter();
                IDbDataParameter PLDist = insCMD.CreateParameter();
                IDbDataParameter AIDist = insCMD.CreateParameter();
                insCMD.Parameters.Add(MatchID);
                insCMD.Parameters.Add(PLShots);
                insCMD.Parameters.Add(AIShots);
                insCMD.Parameters.Add(Time);
                insCMD.Parameters.Add(PLDist);
                insCMD.Parameters.Add(AIDist);

                MatchID.ParameterName = "@MatchID";
                MatchID.DbType = DbType.Int32;
                MatchID.Value = Complete.GameManager.gameManager.MatchID;

                PLShots.ParameterName = "@PLShots";
                PLShots.DbType = DbType.Int32;
                PLShots.Value = _plShots;

                AIShots.ParameterName = "@AIShots";
                AIShots.DbType = DbType.Int32;
                AIShots.Value = _aiShots;

                Time.ParameterName = "@Time";
                Time.DbType = DbType.Double;
                Time.Value = _time;

                PLDist.ParameterName = "@PLDist";
                PLDist.DbType = DbType.Int32;
                PLDist.Value = _plDist;

                AIDist.ParameterName = "@AIDist";
                AIDist.DbType = DbType.Int32;
                AIDist.Value = _aiDist;

                insCMD.ExecuteNonQuery();
                insCMD.Dispose();

                // Update total number of shots
                IDbCommand updCMD = connection.CreateCommand();
                updCMD.CommandText = $"UPDATE TotalShots SET PLShots = PLShots + {_plShots}, AIShots = AIShots + {_aiShots};";
                updCMD.ExecuteNonQuery();
                updCMD.Dispose();

                connection.Close();
            }
        }

        public static void InsertDeath(Vector3 _position)
        {
            using (IDbConnection connection = new SqliteConnection(connectionString))
            {
                connection.Open();
                IDbCommand dbcmd = connection.CreateCommand();
                dbcmd.CommandText = $"INSERT INTO DeathCoord(MatchID, X, Y, Z)" +
                                    $"VALUES (@MatchID, @X, @Y, @Z);";

                IDbDataParameter MatchID = dbcmd.CreateParameter();
                IDbDataParameter X = dbcmd.CreateParameter();
                IDbDataParameter Y = dbcmd.CreateParameter();
                IDbDataParameter Z = dbcmd.CreateParameter();
                dbcmd.Parameters.Add(MatchID);
                dbcmd.Parameters.Add(X);
                dbcmd.Parameters.Add(Y);
                dbcmd.Parameters.Add(Z);

                MatchID.ParameterName = "@MatchID";
                MatchID.DbType = DbType.Int32;
                MatchID.Value = Complete.GameManager.gameManager.MatchID;

                X.ParameterName = "@X";
                X.DbType = DbType.Double;
                X.Value = (double)_position.x;

                Y.ParameterName = "@Y";
                Y.DbType = DbType.Double;
                Y.Value = (double)_position.y;

                Z.ParameterName = "@Z";
                Z.DbType = DbType.Double;
                Z.Value = (double)_position.z;

                dbcmd.ExecuteNonQuery();

                dbcmd.Dispose();
                connection.Close();
            }
        }
    }
}
